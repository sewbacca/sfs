
# Running tests

```bash
# Make sure all dependencies are installed
$ luarocks install --only-deps
# Run tests
$ lua test.lua
# To rerun tests on a posix (Linux/MinGW/Mac) shell, remove the directory first
$ rm playground -rf && lua test.lua
# On use rmdir on Windows
$ rmdir playground /s /q & lua test.lua
```

Removing the directory is necessary to not rely on sfs methods for doing so. A successful run will delete the playground on its own. In an unsuccessfull run the playground is available for inspection.

To test specific functionalities in a more interactive way, run `sfsrepl.lua`
```bash
$ lua sfsrepl.lua 
Simple sfs repl for testing purposes
Type exit for exit
> help
cd
cwd
exit
find
help
ls
> |
```