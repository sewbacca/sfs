
# sfs

`sfs` is an abstraction layer above luafilesystem, to simplify everyday fs operations.

Currently this repository is under development and only used within [lrocket](https://codeberg.org/lrocket/lrc).

### Installation
```bash
$ luarocks install sfs
```