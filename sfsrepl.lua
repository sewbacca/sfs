package.path = "lua/?.lua;"..package.path

local sfs = require "sfs"

sfs.onfs = print

---@diagnostic disable-next-line: deprecated
local unpack = unpack or table.unpack

print("Simple sfs repl for testing purposes")
print("Type exit for exit")

local cmds = {}

---@param str string
---@return table
local function tokenize(str)
	local items = {}

	local i = 1
	local arg = ""
	local inQuote = false
	while true do
		local token =
			str:match('^[^%s"\\]+', i) or
			str:match('^%s+', i) or
			str:match('^\\.', i) or
			str:match('^"', i)

		if not token then
			items[#items+1] = arg
			break
		end

		i = i + #token

		if token == '"' then
			inQuote = not inQuote
		end

		token = token:match("^\\(.)$") or token

		if token:find '^%s+$' and not inQuote then
			items[#items+1] = arg
			arg = ""
		else
			arg = arg..token
		end
	end



	return items
end

function cmds.cd(dir)
	sfs.chdir(dir)
end

function cmds.cwd()
	print(sfs.cwd())
end

function cmds.find(glob)
	for entry in sfs.glob(unpack(tokenize(glob))) do
		print(entry)
	end
end

function cmds.ls(dir)
	for entry in sfs.entries(dir) do
		print(entry)
	end
end

local running = true
function cmds.exit()
	running = false
end

function cmds.help()
	local ks = {}
	for k in pairs(cmds) do
		ks[#ks+1] = k
	end

	table.sort(ks)

	for _, k in ipairs(ks) do print(k) end
end

function cmds.rel(arg)
	local args = tokenize(arg)

	if #args < 2 then
		print("expected <abspath> <relative-dir>")
		return
	end

	print(sfs.relpath(args[1], args[2]))
end

while running do
	io.write "> "
	local cmd, rest = io.read "*l":match "^(%w*)%s*(.*)$"

	if #rest == 0 then
		rest = '.'
	end

	local f = cmds[cmd]

	if not f then
		io.write(cmd, ' not found\n')
	else
		f(rest)
	end
end