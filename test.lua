
package.path = "./lua/?.lua;"..package.path

local lfs = require "lfs"
local sfs = require "sfs"

assert(not sfs.getType "playground", "Delete playground first")

assert(lfs.mkdir "playground")
assert(lfs.chdir "playground")

sfs.onfs = print
sfs.onexec = print

local function assertHasItems(expected, got)
	local map = {}
	for i = 1, #expected do
		map[expected[i]] = true
	end

	local missmatch = { "array missmatch" }
	for i = 1, #got do
		if map[got[i]] then
			map[got[i]] = nil
		else
			missmatch[#missmatch+1] = string.format("\tat index %d: unexpected %s", i, got[i])
		end
	end

	for k in pairs(map) do
		missmatch[#missmatch+1] = string.format("\tmissing %s", k)
	end

	if #missmatch > 1 then
		error(table.concat(missmatch, "\n"), 2)
	end
end

local function assertEquals(b, a)
	if a ~= b then
		error(string.format("expected %s got %s", a, b), 2)
	end
end

sfs.DIR_SEP = '/'

assertEquals(sfs.getExtension "foo/bar.ext", "ext")
assertEquals(sfs.getExtension "foo/bar", nil)
assertEquals(sfs.getBaseName "/foo/bar.ext", "bar")
assertEquals(sfs.getBaseName "/foo/bar", "bar")
assertEquals(sfs.getDir "/foo/bar", "/foo")
assertEquals(sfs.getDir "/foo", "/")
assertEquals(sfs.getDir "foo", ".")
assertEquals(sfs.getDir "./foo", ".")

assertEquals(sfs.relpath("/a/b/c", "/a"), "b/c")
assertEquals(sfs.relpath("a/b/c", "."), "a/b/c")

assertEquals(sfs.normalize "/a/no/../b/c", "/a/b/c")
assertEquals(sfs.normalize "/a/./././b/c", "/a/b/c")
assertEquals(sfs.normalize "", ".")
assertEquals(sfs.normalize "/", "/")

local lineExampleData = "Hello world\nanother line"
local lined = {}
for line in lineExampleData:gmatch "[^\n]+" do
	lined[#lined+1] = line
end

assert(sfs.write("test.dat", lineExampleData))
assertEquals(sfs.read("test.dat"), lineExampleData, "writing / reading to file failed")
assert(sfs.isFile("test.dat"))
assert(not sfs.isDir("test.dat"))

assert(sfs.write("nonbinary.txt", lineExampleData))
assert(sfs.isFile("nonbinary.txt"))
assert(not sfs.isDir("nonbinary.txt"))
do
	local i = 0
	for line in io.lines("nonbinary.txt") do
		i = i + 1
		assertEquals(line, lined[i], "writing /reading non binary failed")
	end
end

assertHasItems({
	"test.dat",
	"nonbinary.txt"
}, sfs.list("."))

assert(sfs.rm("test.dat", "file"))

assertHasItems({
	"nonbinary.txt"
}, sfs.list("."))

assert(sfs.mkdir "directory")
assert(sfs.isDir "directory")
assert(not sfs.isFile "directory")

assert(sfs.mkdir("glob"))


assert(sfs.mkdir(sfs.combine("glob", "ps", "fb")))
assert(sfs.mkdir(sfs.combine("glob", "p1s", "f1b")))
assert(sfs.mkdir(sfs.combine("glob", "p2s", "f1b")))
assert(sfs.mkdir(sfs.combine("glob", "p10s", "f2b")))

local function listglob(...)
	local result = {}
	for item in sfs.glob(...) do
		result[#result+1] = item
	end
	return result
end

assertHasItems({ "glob/ps", "glob/p1s", "glob/p2s", "glob/p10s" }, listglob("glob/*"))
assertHasItems({}, listglob("glob/s*"))
assertHasItems({ "glob/ps", "glob/p1s", "glob/p2s", "glob/p10s" }, listglob("glob/*s"))
assertHasItems({ "glob/ps", "glob/p1s", "glob/p2s", "glob/p10s" }, listglob("glob/p*"))
assertHasItems({ "glob/p1s", "glob/p2s" }, listglob("glob/p?s"))
assertHasItems({ "glob/p1s", "glob/p10s" }, listglob("glob/p{1,10}s"))
assertHasItems({ "glob/p10s" }, listglob("glob/p??s"))
assertHasItems({ "glob/p10s" }, listglob("glob/p??s"))
assertHasItems({ "glob/p1s/f1b", "glob/p2s/f1b" }, listglob("glob/*/f1b"))
assertHasItems({ "glob/p1s/f1b", "glob/p2s/f1b" }, listglob("glob/*/f1b"))
assertHasItems({ "glob/p1s/f1b", "glob/p2s/f1b", "glob/p10s/f2b" }, listglob("glob/*/f?b"))
assertHasItems({ "glob/ps/fb" }, listglob("!**f?b", "glob/*/*"))
assertHasItems({ }, listglob("!**/f?b", "glob/p?s/*") )
assertHasItems({ "glob", "glob/ps", "glob/ps/fb", "glob/p1s", "glob/p2s", "glob/p10s" }, listglob("!**/f?b", "glob/p?s/*", "glob/**") )

assertHasItems({ "glob/p2s", "glob/p10s/f2b" }, listglob("glob/**2?"))

assert(sfs.copy("nonbinary.txt", "directory/nonbinary.txt"))
assert(sfs.isFile "directory/nonbinary.txt")
assertEquals(sfs.read "nonbinary.txt", sfs.read "directory/nonbinary.txt")

assert(sfs.copy("directory", "directory~", "recursive"))
assertHasItems(sfs.list "directory", sfs.list "directory~")

assert(not sfs.rm("directory~"))
assert(not sfs.rm("directory~", "dir"))
assert(sfs.rm("directory~", "recursive"))
assert(not sfs.getType "directory~")

assert(sfs.move("directory", "directory~"))
assert(sfs.rm("directory~/nonbinary.txt"))
assert(sfs.rm("directory~", "dir"))
assert(sfs.shellin("print 'Hello, world!'", "luac", { "-o", "test.luac" }, "-"))
assert("Hello, world!\n", sfs.shellout("lua", "test.luac"))
print(sfs.shellout {
	FOO="BAR",
	"lua -e ", sfs.quoteArgs("print(os.getenv 'FOO')")
} == "BAR\n")

assert(lfs.chdir "..")

assert(sfs.rm("playground", 'recursive'))

assert(sfs.exec("echo", "hello", { ",", "!" }, "world"))
assertEquals(sfs.shellout("echo", "hello", { ",", "!" }, "world"), "hello , ! world\n")

local tmpfile = assert(sfs.tmpfile())
assert(tmpfile:write("Hello world"))
tmpfile:seek "set"
assertEquals(tmpfile:read "*a", "Hello world")

sfs.exit(0)

